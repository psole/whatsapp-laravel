

Pusher.logToConsole = true;
window.Echo = new Echo({
	authEndpoint: 'http://dawjavi.insjoaquimmir.cat/psole/este/laravel/public/broadcasting/auth',
	broadcaster: 'pusher',
	key: '26d08a0e75fedbf1ce58',
	cluster: 'eu',
	encrypted: true,
	logToConsole: true
});



var ido = "";
var bloqueado = 0;

$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	//Consulta Usuaris
	$("#bususers").on("click", function(event){
		event.preventDefault();
		var usernamo =$('#userabus').val();
			$("div.nombres").each(function(){
					if($(this).find("h3.nombre").text().toUpperCase().indexOf(usernamo.toUpperCase()) != -1){
						$(this).show();
					}else{
						$(this).hide();
					}
		})
	});

	//Retorna tots els Usuaris
	$("#mostrarlos").on("click",function () {
		$("div.nombres").each(function(){
			$(this).show();
		});
	});

	//Envia Missatge
	$("#formula").on("submit" , function (event) {
		event.preventDefault();
		var formu = $('#formula').attr("action");
		var missatgio = $('#miss').val();
		var ido = $('.idenv').attr("id");
		$.post(formu,{message: missatgio, ide: ido});
		$('#miss').val("");
	});

	//Missatges Busqueda
	$("#busmiss").on("click", function(event){
		event.preventDefault();
		var formulua = $('#consultam').attr("action");
		var missagios =$('#missabus').val();
		var j=0;
		$.post(formulua, {missabus: missagios, idmi: idm}).done(function(menssages){
			$("#solom").children().remove();
			$('#oculto').hide();
			for(i=0;i<menssages.length;i++){
				var nodeado = document.getElementById("solom");
				divm = document.createElement("div");
				nodeado.appendChild(divm);
				divm.appendChild(document.createTextNode(menssages[i].name+" has said: "+menssages[i].message));
				$(divm).addClass("separa");
			}
		})
	});
	
	//Bloqueja Usuari
	$(".fas.fa-ban").on("click",function(){
		var id = this.id;
		//alert(this.id);
		$.post('home/bloqueja',{idm: idm, ido:id}).done(function(data){
		});
		console.log($(this).attr("style"));
		if($(this).attr("style") == "color: red;"){
			$(this).css("color", "grey");
			$('#miss').attr('readonly', false);
			$('#miss').val("");
			$('#enviados').attr('disabled',false);
			bloqueado=0;
		}else{
			$(this).css("color", "red");
			$('#miss').attr('readonly', true);
			$('#miss').val("");
			$('#enviados').attr('disabled',true);
			bloqueado=1;
		}
	})

	//Mostra missatges per usuari i usuari seleccionat
	$("h3.nombre").on('click', function(event){
		event.preventDefault();
		ido = $(this).attr("id");
		$.post("home/comprovablock", {ido : ido,idm : idm}).done(function(data){
			if(data.bloquejat[0]!=null){
				var userblock = data.bloquejat[0].id_bloquejat;		
				$('#miss').attr('readonly', true);
				bloqueado = userblock;
				$('#miss').val("");
				$('#enviados').attr('disabled',true);
			}else{
				$('#miss').attr('readonly', false);
				$('#miss').val("");
				$('#enviados').attr('disabled',false);
				bloqueado=0;
			}
		})
		$.post("home/chatp", {ido : ido,idm : idm}).done(function(data){
			$("#solom").children().remove();
			for(i=0;i<data.length;i++){
				var nodeado = document.getElementById("solom");
				divm = document.createElement("div");
				nodeado.appendChild(divm);
				divm.appendChild(document.createTextNode(data[i].name+" has said: "+data[i].message));
				$(divm).addClass("separa");
			}
		})
		$('.idenv').attr("id",ido);
		
		$('#oculto').show();
	});

	//Usuaris connectats/desconnectats
	Echo.join('chat')
    .here((users) => {
		for(i=0;i<users.length;i++){
			$(".nombre").each(function(){
				if( users[i].name == $(this).text()){
					$("#icono"+users[i].id).css("color", "green");
				}
			});
		}
    })
    .joining((users) => {
		$(".nombre").each(function(){
			if( users.name == $(this).text()){
				$("#icono"+users.id).css("color", "green");
			}
		});
    })
    .leaving((users) => {
		$(".nombre").each(function(){
			if( users.name == $(this).text()){
				$("#icono"+users.id).css("color", "grey");
			}
		});
	});
	
	//whisper
	$("#miss").on("keydown", function(){
			var id = $(".idenv").attr("id");
					Echo.private('pri')
						.whisper('typing', {
							name: nombre,
							id: id
						});
		});
	Echo.private('pri')
    .listenForWhisper('typing', (e) => {
			var prueba = idm;
			var idenv1 = e.id;
			if(bloqueado==0){
				if(prueba == idenv1){
					//var nodeado2 = document.getElementById("escriu");
					$("#escriu").html(e.name+" is typing...");
					$("#escriu").show();
					setTimeout(function(){$("#escriu").fadeOut()},2000);
				}
			}
	});

	//Mostra nou missatge
	Echo.private('pri').listen('NewMessageNotificationPri', (e) => {
		
		if(idm == e.message.to_id && ido == e.message.from_id && e.message.bloquejat==0 || idm == e.message.from_id && ido == e.message.to_id ){
			var nodeado = document.getElementById("solom");
			divm = document.createElement("div");
			nodeado.appendChild(divm);
			divm.appendChild(document.createTextNode(e.message.name+" has said: "+e.message.message));
			$(divm).addClass("separa");
		}
	});




	//Muro normal

	$("#enviado").on("click" , function (event) {
		event.preventDefault();
		var formu = $('#formul').attr("action");
		var missatgio = $('#miss').val();
		$.post(formu,{message: missatgio});
	});
	Echo.private('TheWall').listen('NewMessageNotification', (e) => {
		var nodeado = document.getElementById("solom");
		divm = document.createElement("div");
		nodeado.appendChild(divm);
		divm.appendChild(document.createTextNode(e.message.name+" has said: "+e.message.message));
		$(divm).addClass("separa");
	});
	
	Echo.private('TheWall')
    .listenForWhisper('typing', (e) => {
		console.log(e.name);
		//var nodeado2 = document.getElementById("escriu");
		$("#escriu").html(e.name+" is typing...");
		$("#escriu").show();
		setTimeout(function(){$("#escriu").fadeOut()},2000);
		
	});
	
	
})

