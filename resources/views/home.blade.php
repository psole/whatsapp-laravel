@extends('layouts.app')

    
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h4>Busca usuaris:</h4>
                    <form action="home/consultausers" method="post" id="consultau">
                        <input type="text" id="userabus" name="userabus">
                        <button id="bususers">Busca</button>
                    </form>
                    <button class="separa2" id="mostrarlos">Retorna tots els usuaris</button>
                    <h4 class="separa2">Busca missatges:</h4>
                    <form action="home/consultamissatges" method="post" id="consultam">
                        <input class="separa2" type="text" id="missabus" name="missabus">
                        <button id="busmiss">Busca</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
                <h2>Usuaris:</h2>
                @foreach($users as $user)
						<div class="nombres">
							<h3 class="nombre" id="{{ $user->id }}">{{$user->name}}</h3>
                            <i class= 'fas fa-user-circle' id="icono{{ $user->id }}"> </i> 
                        @if($bloquejats!=null)
                            @foreach($bloquejats as $bloquejat)
                                @if(str_contains($bloquejat->id_bloquejat,$user->id))
                                    <i class= 'fas fa-ban' id="{{ $user->id }}" style="color:red;"></i>
                                    @break
                                @else
                                    @if($loop->iteration==$loop->last)
                                        <i class= 'fas fa-ban' id="{{ $user->id }}" style="color:grey;"></i>
                                    @endif
                                @endif   
                            @endforeach
                        @else
                            <i class= 'fas fa-ban' id="{{ $user->id }}" style="color:grey;"></i>
                        @endif
                        </div>
			    @endforeach        
        </div>
        <div class="col-md-4">
                <h2>Chat:</h2>
                <div id="solom">
  
                </div>
                <div id="oculto" style="display: none;">
                    <form action="home/enviar" method="post" id="formula">
                            @csrf
                            <input type="text" id="miss" name="message">
                            <input type="hidden" class="idenv" id="1" name="ide"> 
                            <button id="enviados">Envia</button>
                    </form>
                </div>
                <div id="escriu"></div>
        </div>
    </div>
</div>
<script>
    var idm = {{Auth::user()->id}};
</script>
<script>
	var nombre = "{{Auth::user()->name}}"; 
</script>
<script src="{{ asset('js/mandame.js') }}" ></script>
@endsection

