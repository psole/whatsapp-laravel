<!DOCTYPE html>

	<html lang="{{ app()->getLocale() }}">

	<head>

	    <meta charset="utf-8">

	    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	    <meta name="viewport" content="width=device-width, initial-scale=1">

	 	<link href="{{asset('css/mandame.css')}}" rel="stylesheet">

	    <!-- CSRF Token -->

	    <meta name="csrf-token" content="{{ csrf_token() }}">

		<script
		src="https://code.jquery.com/jquery-3.3.1.js"
		integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
		crossorigin="anonymous"></script>

	    <title>Donald Trumpo's Wall</title>

            <script>

                window.Laravel = <?php echo json_encode([

                'csrfToken' => csrf_token(),

                ]); ?>;

                var module = { }; /*   <-----THIS LINE */

            </script>

	    <!-- Styles -->

	    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>


	</head>
    <body>
	@extends('layouts.app')

		@section('content')
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<h1>Welcome to larachat</h1>
						<form action="envia" method="post" id="formul">
							@csrf
							<input type="text" id="miss" name="message">
							<button id="enviado">Envia</button>
						</form>
						<div id="escriu"></div>
					</div>
					<div id="solom" class="sulom">
						
						@foreach($missatges as $missatge)
							<div class="separa">
								{{$missatge->name}} has said: {{$missatge->message}}
							</div>
						@endforeach
						
					</div>
				</div>
			</div>
		</div>
		@endsection
		<script>
				var nombre = "{{Auth::user()->name}}"; 
		</script>
		<script src="{{asset('js/echo.js')}}"></script>
		<script src="{{asset('js/mandame.js')}}"></script>
    </body>

</html>