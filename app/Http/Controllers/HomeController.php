<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Events\NewMessageNotificationPri;
use App\Message_pri;
use App\Bloquejat;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $users = User::where('id', '!=', auth()->id())->get();
        $user_id = Auth::user()->id;
        
        $bloqueado = DB::select('select id_bloquejat from Bloquejats where id_usuari='.$user_id);

        $data = array('users' => $users,'bloquejats' =>$bloqueado);

        return view('home',$data);
    }

    public function mostrap(Request $request)
    {
        $user_id = $request -> input("idm");
        $user_od = $request -> input("ido");
        
        $messages = DB::select('select message,name from Messages_pri where ((to_id = '.$user_id.' and from_id = '.$user_od.' and bloquejat=0) or (to_id = '.$user_od.' and from_id = '.$user_id.'))');
        
        return response()->json($messages);
    }

    public function comprovablock(Request $request){
        $user_id = $request -> input("idm");
        $user_od = $request -> input("ido");

        $bloqueado = DB::select('select id_usuari, id_bloquejat from Bloquejats where id_usuari='.$user_id.' and id_bloquejat='.$user_od);

        $data = array('bloquejat' => $bloqueado);

        return response()->json($data);

    }

    public function conmissa(Request $request){

        $missangio = $request -> input("missabus");

        $idmio = $request -> input("idmi");

        $menssages = DB::select('select message,name from Messages_pri where message like "%'.$missangio.'%" and (to_id = '.$idmio.' or from_id = '.$idmio.')');

        return response()->json($menssages);
    }
    public function bloqueja(Request $request)
    {
        $idmio = $request -> input("idm");
        $idbloquea = $request -> input("ido");
        $bloqueado = DB::select('select ID from Bloquejats where id_usuari='.$idmio.' and id_bloquejat='.$idbloquea);
        if($bloqueado!=null){
            Bloquejat::where("id_usuari", $idmio)->where("id_bloquejat",$idbloquea)->delete();
            $data="Aquesta persona ja no esta bloquejada";
            return response()->json($data);
        }else{
            $bloquea = new Bloquejat; 

            $bloquea->setAttribute('id_usuari',$idmio);
            $bloquea->setAttribute('id_bloquejat',$idbloquea);
            $bloquea->save();
            $data="Aquesta persona ha estat bloquejada";
            return response()->json($data);
        }
    }

    public function sendo(Request $request)

    {
        // ...

        $user_id = Auth::user()->id;
        $user_to_id = $request->input("ide");
        $user_name = Auth::user()->name;
        $missatge = $request->input("message");

        // message is being sent
        $bloqueado = DB::select('select ID from Bloquejats where id_usuari='.$user_to_id.' and id_bloquejat='.$user_id);

        if($bloqueado != null){
            $message = new Message_pri;

            $message->setAttribute('name', $user_name);

            $message->setAttribute('message', $missatge);

            $message->setAttribute('from_id', $user_id);

            $message->setAttribute('to_id', $user_to_id);

            $message->setAttribute('bloquejat', 1);
        }else{

            $message = new Message_pri;

            $message->setAttribute('name', $user_name);

            $message->setAttribute('message', $missatge);

            $message->setAttribute('from_id', $user_id);

            $message->setAttribute('to_id', $user_to_id);

            $message->setAttribute('bloquejat', 0);
        }
        

        $message->save();

         

        // want to broadcast NewMessageNotification event

        event(new NewMessageNotificationPri($message));


        // ...

    }

    /*public function conusers(Request $request)
    {
        $namo = $request -> input("userabus");

        $userarios = DB::select('select name from users where name like "%'.$namo.'%"');

        return response()->json($userarios);

    }*/
}
