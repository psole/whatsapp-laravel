<?php

namespace App\Http\Controllers;

 

use App\Http\Controllers\Controller;

use App\Message;

use App\Events\NewMessageNotification;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request; 

class MessageController extends Controller

{

    public function __construct() {

        $this->middleware('auth');

    }



    public function indexo()

    {

        $user_id = Auth::user()->id;

        $missatges = Message::all();

        $data = array('user_id' => $user_id, 'missatges' => $missatges);

        return view('chat1', $data);

    }

    public function sendo(Request $request)

    {

        // ...

        $user_id = Auth::user()->id;
        $user_name = Auth::user()->name;
        $missatge = $request->input("message");

        // message is being sent

        $message = new Message;

        $message->setAttribute('from', $user_id);

        $message->setAttribute('to', $user_id);

        $message->setAttribute('message', $missatge);

        $message->setAttribute('name', $user_name);

        $message->save();

         

        // want to broadcast NewMessageNotification event

        event(new NewMessageNotification($message));

         

        // ...

    }
}

