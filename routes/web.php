<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/home/chatp', 'HomeController@mostrap');

Route::post('/home/comprovablock', 'HomeController@comprovablock');

Route::post('/home/enviar', 'HomeController@sendo');

//Route::post('/home/consultausers','HomeController@conusers');

Route::post('/home/consultamissatges','HomeController@conmissa');

Route::post('/home/bloqueja', 'HomeController@bloqueja');


Route::get('missatge/index','MessageController@indexo');

Route::post('missatge/envia','MessageController@sendo');